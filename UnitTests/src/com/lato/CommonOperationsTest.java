package com.lato;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;


import static org.testng.Assert.*;

public class CommonOperationsTest {

    @DataProvider(name = "testFloorRootValueDataProvider")
    public static Object[][] numbers2() {
        return new Object[][] {{5,2}, {9,3}};
    }

    @Test(dataProvider = "testFloorRootValueDataProvider")
    public void floorRootValue_returnsCorrectResult_correctParameterGiven(int number, int expResult) {
        System.out.println("floorRootValue");
        CommonOperations instance = new CommonOperations();
        int result = instance.floorRootValue(number);
        assertEquals(expResult, result);
    }


    @Test()
    public void isDivisible_true_isDivisible() {
        System.out.println("isDivisible");
        int divident = 4;
        int divisor = 3;
        CommonOperations instance = new CommonOperations();
        boolean expResult = false;
        boolean result = instance.isDivisible(divident, divisor);
        assertEquals(expResult, result);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testIsDivisible_throwsExceptions_whileDividingByZero() {
        int divisor = 0;
        int divident = 4;
        CommonOperations co = new CommonOperations();
        boolean result = co.isDivisible(divident, divisor);
    }

}