package com.lato;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PrimeNumbersTest {

    @Test
    public void createTable_returnArray_numberOfColumnsPassed() {
        System.out.println("createTable");
        int x = 5;
        PrimeNumbers instance = new PrimeNumbers();
        int[][] expResult = {{2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}};
        int[][] result = instance.createTable(x);
        assertEquals(result, expResult);
//        fail("The test case is a prototype.");
    }


    @Test
    public void testMarkNonPrimeFromTable_marksNonPrime_whenNotDivisible() {
        System.out.println("markNonPrimeFromTable");
        int[][] numbersTable = {{2,1}, {3,1}, {4,1}, {5,1}};
        int range = 2;
        int[][] expResult = {{2,1}, {3,0}, {4,0}, {5,0}};
        PrimeNumbers pm = new PrimeNumbers(new CommonOperations() {
            @Override
            boolean isDivisible(int divident, int divisor) {
                return true;
            }
        });
        int[][] result = pm.markNonPrimeFromTable(numbersTable, range);
        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
    }




























//    @Test
//    public void testShowPrimeNumbers() {
//        System.out.println("showPrimeNumbers");
//        int[][] numbersTable = {{2, 1}, {3, 1}};
//        PrimeNumbers instance = new PrimeNumbers();
//        instance.showPrimeNumbers(numbersTable);
//        fail("The test case is a prototype.");
//    }


//    @Test
//    public void testMain() {
//        System.out.println("main");
//        String[] args = null;
//        PrimeNumbers.main(args);
//        fail("The test case is a prototype.");
//    }


}
