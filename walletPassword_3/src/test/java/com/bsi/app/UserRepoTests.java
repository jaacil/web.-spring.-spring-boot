package com.bsi.app;

import com.bsi.app.model.User;
import com.bsi.app.repo.UserRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)

public class UserRepoTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepo repo;

    @Test
    public void testCreateUser() {
        User user = new User();
        user.setEmail("jacek@example.com");
        user.setLogin("Jacek");
        user.setPassword("TestPassWD");
        user.setSalt("abcd");
        user.setIsPasswordKeptAsHash(true);

        User savedUser = repo.save(user);

        User exitUser = entityManager.find(User.class, savedUser.getId());
        assertThat(user.getEmail()).isEqualTo(exitUser.getEmail());

    }

}
