package com.bsi.app.service;

import org.testng.annotations.Test;

import java.security.Key;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AESencTest {

    final private String examplePassword = "examplePassword";

    final private byte[] expResult = {106, -114, 75, -64, 17, 91, 21, -43, 46, -62, 11, 63, 56, 19, 112, 70};

    final private String expALGO = "AES";

    final private String expEncryptedPassword = "XJbh9KC4Vraoe5xy134o9Q==";

    @Test
    public void calculateMD5_returnMessageDigest_passwordPassed() {
        System.out.println("calculateMD5");
//      1. calculate value  2. save in expResult 3. assert with real result
        byte[] result = AESenc.calculateMD5(examplePassword);
        assertEquals(expResult, result);
    }

    @Test
    public void generateKey_returnNotNull_passwordPassed() throws Exception {
        System.out.println("generateKey");
        Key key = AESenc.generateKey(examplePassword);
        assertNotNull(key);
    }

    @Test
    public void generateKey_returnKey_passwordForKeyPassed() throws Exception {
        System.out.println("generateKey");
        Key key = AESenc.generateKey(examplePassword);
        assertEquals(key.getEncoded(), expResult);
        assertEquals(key.getAlgorithm(), expALGO);
    }

    @Test
    public void encrypt_returnEncryptedPassword_plainTextPasswordPassed() throws Exception {
        System.out.println("encrypt");
        Key key = AESenc.generateKey(examplePassword);
        String result = AESenc.encrypt(examplePassword, key);
        assertEquals(expEncryptedPassword, result);
    }

    @Test
    public void decrypt_returnPlainText_encryptedPasswordPassed() throws Exception {
        System.out.println("decrypt");
        Key key = AESenc.generateKey(examplePassword);
        String result = AESenc.decrypt(expEncryptedPassword, key);
        assertEquals(examplePassword, result);
    }




}