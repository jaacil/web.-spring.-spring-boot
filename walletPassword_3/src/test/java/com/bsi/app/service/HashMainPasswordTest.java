package com.bsi.app.service;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HashMainPasswordTest {

    final private String examplePassword = "examplePassword";
    final private String exampleKey = "exampleKey";

    final private String expResultSHA = "a70c8f574ee0be215d8bbb584b122abd20da366c545b04eac88c23ff3a4257e8f2c06f5ff9a7bfcd79f3fc3cbb205ce8ade9ba44741eb8fc2d4cd1a84efda8a";

    final private String expResultHMAC = "4e75f4ca9383111e79ce484460a2817c30966f2bb1e09a7d9a159b93211aec2a8351db36487e6971518db94e176fff2c7d6a1aabe3e11d2e46eef3c5d7bcd357";

    final private String expResultHex = "6578616d706c6550617373776f7264";

    @Test
    public void calculateSHA512_returnCalculatedHash_plaintTextAndSaltPassed() {
        System.out.println("calculateSHA512");
        String result = HashMainPassword.calculateSHA512(examplePassword);
        assertEquals(expResultSHA, result);
    }

    @Test
    public void calculateHMAC2_returnCalculatedHash_plainTextAndKeyKeyPassed() {
        System.out.println("calculateHMAC2");
        String result = HashMainPassword.calculateHMAC2(examplePassword, exampleKey);
        assertEquals(expResultHMAC, result);
    }

    @Test
    public void toHexString_returnValueInHex_byteValuePassed() {
        System.out.println("toHexString");
        String result = HashMainPassword.toHexString(examplePassword.getBytes());
        assertEquals(expResultHex, result);
    }
}