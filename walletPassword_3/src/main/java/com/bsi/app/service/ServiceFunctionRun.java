package com.bsi.app.service;

import com.bsi.app.model.FunctionRun;
import com.bsi.app.repo.FunctionRunRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceFunctionRun {

    @Autowired
    private FunctionRunRepo functionRunRepo;

    public void save(FunctionRun functionRun) {
        functionRunRepo.save(functionRun);
    }

    public List<FunctionRun> findAllByUserId(Long id) {
        return functionRunRepo.findAllByUserId(id);
    }

}
