package com.bsi.app.service;

import com.bsi.app.model.Sites;
import com.bsi.app.repo.SitesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//Service class for sites
//it is logical layer of app

@Service
public class ServiceSites {

    @Autowired
    private SitesRepo sitesRepo;

    public Optional<Sites> findById(Long id) {
     return sitesRepo.findById(id);
    }

    public List<Sites> findByUserId(Long id) {
        return sitesRepo.findByUserId(id);
    }

    public List<Sites> findAllByUserId(Long id) {
        return sitesRepo.findAllByUserId(id);
    }

    public void save(Sites site) {
        sitesRepo.save(site);
    }

    public void deleteById(Long id) {
        sitesRepo.deleteById(id);
    }

    public Sites findSiteById(Long id) {
        return sitesRepo.findById(id).get();
    }

}
