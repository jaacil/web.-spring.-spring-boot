package com.bsi.app.service;


import com.bsi.app.model.Function;
import com.bsi.app.repo.FunctionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceFunction {

    @Autowired
    private FunctionRepo functionRepo;

    public Function findByFunctionName(String name) {
        return functionRepo.findByFunctionName(name);
    }
}
