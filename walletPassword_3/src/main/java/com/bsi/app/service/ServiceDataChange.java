package com.bsi.app.service;

import com.bsi.app.model.DataChange;
import com.bsi.app.repo.DataChangeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceDataChange {

    @Autowired
    private DataChangeRepo dataChangeRepo;

    public void save(DataChange dataChange) {
        dataChangeRepo.save(dataChange);
    }

    public List<DataChange> findAllByUserId(Long id) {
        return dataChangeRepo.findAllByUserId(id);
    }

}
