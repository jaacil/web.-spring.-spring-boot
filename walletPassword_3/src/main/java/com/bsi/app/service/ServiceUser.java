package com.bsi.app.service;

import com.bsi.app.model.User;
import com.bsi.app.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//Service class for users
//it is logical layer of app

@Service
public class ServiceUser {

    @Autowired
    UserRepo userRepo;

    public void save(User user) {
        userRepo.save(user);
    }

    public List<User> findUserByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    public List<User> findUserByLogin(String login) {
        return userRepo.findByLogin(login);
    }

    public User findUserById(Long id) {
        return userRepo.findById(id).get();
    }

}
