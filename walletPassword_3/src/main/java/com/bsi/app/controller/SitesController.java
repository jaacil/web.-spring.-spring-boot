package com.bsi.app.controller;

import com.bsi.app.model.Function;
import com.bsi.app.model.FunctionRun;
import com.bsi.app.model.Sites;
import com.bsi.app.model.User;
import com.bsi.app.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Key;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.bsi.app.controller.UserController.modifyMode;
import static com.bsi.app.controller.UserController.userid;

@Controller
@Scope("session")
public class SitesController {

    @Autowired
    private ServiceSites serviceSites;

    @Autowired
    private ServiceUser serviceUser;

    @Autowired
    private ServiceFunctionRun serviceFunctionRun;

    @Autowired
    private ServiceFunction serviceFunction;

    public static String masterPassword = null;

    public static Long siteid;

    //this method ask for master password from user
    @RequestMapping (value = "/processPassDecoding", method = RequestMethod.POST)
    public String takeMasterPassword(@RequestParam("password") String password) {
        masterPassword = password;
        return "redirect:/showSites";
    }

    //this method shows user a page to adding new site to wallet
    @GetMapping("/addSite")
    public String showAddSiteForm(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("siteObj", new Sites());

        if(masterPassword == null) {
            redirectAttributes.addFlashAttribute("error", "You didn't input your master password!");
            return "redirect:/showSites";
        }

        if(userid == null) {
            return "login";
        } else {
            return "add_site";
        }
    }

    //this method displays for user his list of sites with passwords
    @GetMapping("/showSites")
    public String getAllSitesByUserId(Model model) {
        if(userid==null) {
            return "login";
        } else {
            List<Sites> sites = serviceSites.findAllByUserId(userid);
            model.addAttribute("sites" , sites);
            return "show_sites_list";
        }
    }

    //this method saves new site into database, also hashes password
    @PostMapping("/addNewSite")
    public String processSiteAdding(Sites site, RedirectAttributes redirectAttributes) {

        if(userid == null) {
            return "login";
        }

        if(masterPassword == null) {
            redirectAttributes.addFlashAttribute("error", "You didn't input your master password!");
            return "redirect:/showSites";
        }

        User user = serviceUser.findUserById(userid);
        site.setUser(user);
        String functionName = "Create";
        try{
            Key key  = AESenc.generateKey(masterPassword); //returns key - using calculateMD5
            String password = AESenc.encrypt(site.getPassword(), key); //calculate hash from entered password
            site.setPassword(password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        serviceSites.save(site);
        saveLog(functionName);
        return "redirect:/showSites";
    }

    //this method decodes sites password using master password of user
    @GetMapping("/decodePassword/{id}")
    public String processDecodingPassword(@PathVariable("id") Long id, RedirectAttributes redirectAttributes, Model model) {

        if(masterPassword == null) {
            redirectAttributes.addFlashAttribute("error", "You didn't input your master password!");
            return "redirect:/showSites";
        }
        Sites site = serviceSites.findSiteById(id);
        model.addAttribute("thisSite", site);
        String functionName = "View";
        try{
            Key key = AESenc.generateKey(masterPassword);
            String decoded = AESenc.decrypt(site.getPassword(), key);
            model.addAttribute("decoded", decoded);
            saveLog(functionName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "show_decoded";
    }

    //this method shows user form where he can edit existing site
    @GetMapping("/editSite/{id}")
    public String showEditSiteForm(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {

        if(userid == null) {
            return "login";
        }

        if(masterPassword == null) {
            redirectAttributes.addFlashAttribute("error", "You didn't input your master password!");
            return "redirect:/showSites";
        }

        Sites sites = serviceSites.findSiteById(id);
        siteid = id;

        if(!modifyMode) {
            redirectAttributes.addFlashAttribute("error", "You can not edit in read mode, please enter modify mode!");
        } else {
            try {
                Key key = AESenc.generateKey(masterPassword);
                String password = AESenc.decrypt(sites.getPassword(), key);
                model.addAttribute("passwordDec", password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.addAttribute("siteEdit", sites);
            return "edit_site";
        }

        return "redirect:/showSites";
    }

    //this method save changed details of existing site (ofc also with password)
    @PostMapping("/processSiteEdit")
    public String processSiteEdit(Sites site, RedirectAttributes redirectAttributes) {
        if(userid == null) {
            return "login";
        }

        if(masterPassword == null) {
            redirectAttributes.addFlashAttribute("error", "You didn't input your master password!");
            return "redirect:/showSites";
        }

        Sites siteToEdit = serviceSites.findSiteById(siteid);
        String functionName = "Site update";
        User user = serviceUser.findUserById(userid);

        siteToEdit.setId(siteid);
        siteToEdit.setSite(site.getSite());
        siteToEdit.setDescription(site.getDescription());
        siteToEdit.setUser(user);

        if(!modifyMode) {
            redirectAttributes.addFlashAttribute("error", "You can not edit in read mode, please enter modify mode!");
        } else {
            try {
                Key key = AESenc.generateKey(masterPassword);
                String password = AESenc.encrypt(site.getPassword(), key);
                siteToEdit.setPassword(password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            serviceSites.save(siteToEdit);
            saveLog(functionName);
        }

        return "redirect:/showSites";
    }


    //this method allows user to delete existing site
    @GetMapping("/deleteSite/{id}")
    public String deleteSiteFormList(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

        List<Sites> sitesOfUser = serviceSites.findAllByUserId(userid);
        String functionName = "Delete";
        Optional<Sites> result = sitesOfUser.stream().filter(e -> {
            if(e.getId().equals(id)) {
                return true;
            } else {
                return false;
            }
        }).findFirst(); //szukanie gdzie id strony pasuje do przeslanego id, unikniecie sytuacji gdzie jeden uzytkownik wprowadza id sita innego uzytkownika

        if(!modifyMode) {
            redirectAttributes.addFlashAttribute("error", "You can not delete in read mode, please enter modify mode!");
        } else {
            if(result.get() != null) {
                serviceSites.deleteById(id);
                saveLog(functionName);
            } else {
                return "";
            }
        }
        return "redirect:/showSites";
    }

    @PostMapping("/enterModifyMode")
    public String enterModifyMode(RedirectAttributes redirectAttributes) {
        modifyMode = true;
        redirectAttributes.addFlashAttribute("success", "You have entered into modify mode!");
        return "redirect:/showSites";
    }

    @GetMapping("/showLogs")
    public String getUserLogs(Model model) {
        if(userid==null) {
            return "login";
        } else {
            List<FunctionRun> logs = serviceFunctionRun.findAllByUserId(userid);
            model.addAttribute("logs" , logs);
            return "showLogs";
        }
    }

    public void saveLog(String functionName) {

        User user = serviceUser.findUserById(userid);
        Function function = serviceFunction.findByFunctionName(functionName);

        FunctionRun functionRun = new FunctionRun();
        functionRun.setUser(user);
        functionRun.setFunction(function);
        functionRun.setLocalDate(LocalDateTime.now());

        serviceFunctionRun.save(functionRun);
        System.out.println("Zapisuje nowy log " + functionName);
    }

}
