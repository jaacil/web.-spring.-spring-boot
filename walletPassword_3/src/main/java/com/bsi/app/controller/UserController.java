package com.bsi.app.controller;


import com.bsi.app.model.*;
import com.bsi.app.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Key;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static com.bsi.app.controller.SitesController.masterPassword;

@Component
@Scope("session")
@Controller
public class UserController {

    @Autowired
    private ServiceUser serviceUser;

    @Autowired
    private ServiceSites serviceSites;

    @Autowired
    private ServiceDataChange serviceDataChange;

    @Autowired
    private ServiceFunctionRun serviceFunctionRun;

    @Autowired
    private ServiceFunction serviceFunction;

    public static Long userid;
    public static boolean readMode;
    public static boolean modifyMode;

    //method for displaying main page
    @GetMapping("")
    public String viewHomePage() {
        return "index";
    }

    //method for displaying home(main) page
    @GetMapping("/home")
    public String home() {
        return "index";
    }

    //method for displaying register page
    @GetMapping("/register")
    public String showRegistrationForm(Model model){
        if(userid == null) {
            model.addAttribute("user", new User());
            return "register";
        } else {
            return "login_success_menu";
        }
    }

    //method for saving new user into data base. It hash password like user wanted SHA with salt or HMAC with key
    @PostMapping("/userRegister")
    public String processUserRegister(User user) {

        if(userid != null) {
            return "login_success_menu";
        }

        List<User> existingUser_email = serviceUser.findUserByEmail(user.getEmail()); //if exist
        List<User> existingUser_login = serviceUser.findUserByLogin(user.getLogin()); //if login exist

        if(existingUser_email.isEmpty() && existingUser_login.isEmpty()) {

            boolean isHashed = user.getIsPasswordKeptAsHash();
            String encodedPassword;

            if (isHashed) {
                //hash with salt
                encodedPassword = HashMainPassword.calculateSHA512(user.getPassword() + user.getSalt());
                user.setPassword(encodedPassword);
            } else {
                //hash with secret key
                encodedPassword = HashMainPassword.calculateHMAC2(user.getPassword(), user.getSalt());
                user.setPassword(encodedPassword);
            }
            serviceUser.save(user);
        } else {

        }
        return "register_success";
    }

    //this method show user form for change details
    @GetMapping("/changeDetails")
    public String showChangeDetailsForm(Model model){
        if(userid == null) {
            return "login";
        } else {
            User userToEdit = serviceUser.findUserById(userid);
            model.addAttribute("user", new User());
            model.addAttribute("userToEdit", userToEdit);

            List<DataChange> dataChangeList = serviceDataChange.findAllByUserId(userid);
            model.addAttribute("records", dataChangeList);

            return "change_user_details";
        }
    }

    //this method saves new user detail into database (update)
    @PostMapping("/processChangeUser")
    public String processChangeUserDetails(User user) {

        if(userid == null) {
            return "login";
        }

        User userToEdit = serviceUser.findUserById(userid);

        String encodedPassword;
        String decodedOldSitePassword;

        user.setId(userToEdit.getId());
        user.setEmail(userToEdit.getEmail());
        user.setLogin(userToEdit.getLogin());
        user.setSalt(user.getSalt());
        user.setIsPasswordKeptAsHash(userToEdit.getIsPasswordKeptAsHash());

        String newMasterPassword = user.getPassword();

        if(userToEdit.getIsPasswordKeptAsHash()) {
            encodedPassword = HashMainPassword.calculateSHA512(user.getPassword() + user.getSalt());
            user.setPassword(encodedPassword);
        } else {
            encodedPassword = HashMainPassword.calculateHMAC2(user.getPassword(), user.getSalt());
            user.setPassword(encodedPassword);
        }

        List<Sites> sitesList = serviceSites.findAllByUserId(userToEdit.getId());

        //odkodowane hasel z walleta starym main paswordem i zakodowane nowym
        for (Sites site : sitesList) {
            String oldPassword = site.getPassword();
            try {
                Key oldMainPasswordKey = AESenc.generateKey(masterPassword);
                decodedOldSitePassword = AESenc.decrypt(oldPassword, oldMainPasswordKey);
                Key newMainPasswordKey = AESenc.generateKey(newMasterPassword);
                String newEncodedPassword = AESenc.encrypt(decodedOldSitePassword, newMainPasswordKey);
                site.setPassword(newEncodedPassword);
                serviceSites.save(site);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        serviceUser.save(user);
        //add password changes with action registration
        registerPasswordChange(userid, newMasterPassword);
        masterPassword = newMasterPassword;


        return "login_success_menu";
    }

    //this method shows login page
    @GetMapping("/login")
    public String viewLoginPage() {
        if(userid == null) {
            return "login";
        } else {
            return "login_success_menu";
        }
    }

    //this method shows menu page
    @GetMapping("/showMenu")
    public String viewMenu() {
        if(userid == null) {
            return "login";
        } else {
            return "login_success_menu";
        }
    }

    //this method allow user to log out
    @GetMapping("/logOut")
    public String logOutProcess() {
        if(userid != null) {
            userid = null;
            masterPassword = null;
            modifyMode = false;
        }
        return "index";
    }

    //this method process user login - check if exists, check password and salt/key
    @RequestMapping(value = "/loginForm", method = RequestMethod.POST) //it can be just PostMapping - like changeUser (th:field and just check from DB if correct, no parameter request)
    public String processUserLogin(@RequestParam("password") String password, @RequestParam("email") String email, @RequestParam("salt") String salt, RedirectAttributes redirectAttributes) {

        boolean loginCorrect;
        boolean isHash;
        String pass;
        String passwordInDB;
        String saltInDB;
        List<User> findUserByEmail =  serviceUser.findUserByEmail(email);

        if(findUserByEmail.size() == 0) {
            redirectAttributes.addFlashAttribute("errorNoUser", "User does not exist!");
            return "redirect:/login";
        } else {
            userid = findUserByEmail.get(0).getId();
            passwordInDB = findUserByEmail.get(0).getPassword();
            isHash = findUserByEmail.get(0).getIsPasswordKeptAsHash();
            saltInDB = findUserByEmail.get(0).getSalt();
        }

            if(isHash) {
                pass = HashMainPassword.calculateSHA512(password+salt);
                if(pass.equals(passwordInDB) && salt.equals(saltInDB)) {
                    loginCorrect = true;
                } else {
                    loginCorrect = false;
                    userid = null;
                }
            } else {
                pass = HashMainPassword.calculateHMAC2(password, salt);
                if(pass.equals(passwordInDB) && salt.equals(saltInDB)) {
                    loginCorrect = true;
                } else {
                    loginCorrect = false;
                    userid = null;
                }
            }

            if(loginCorrect) {
                redirectAttributes.addFlashAttribute("success", "Logged in!");
                readMode = true;
                modifyMode = false;
                return "login_success_menu";
            } else {
                redirectAttributes.addFlashAttribute("errorWrongCredentials", "E-mail or password incorrect!");
                return "redirect:/login";
            }
    }

    public void registerPasswordChange(Long id, String newPassword) {
        String oldPassword = masterPassword;
        System.out.println("Id usera: " + id + ", jego stare haslo: " + oldPassword + ", jego nowe haslo: " + newPassword);


        String functionName = "Password update";
        User user = serviceUser.findUserById(id);
        Function function = serviceFunction.findByFunctionName(functionName);

        DataChange dataChange = new DataChange();
        dataChange.setUser(user);
        dataChange.setFunction(function);
        dataChange.setLocalDateTime(LocalDateTime.now());
        dataChange.setOldValue(oldPassword);
        dataChange.setNewValue(newPassword);

        FunctionRun newFunctionRun = new FunctionRun();
        newFunctionRun.setUser(user);
        newFunctionRun.setFunction(function);
        newFunctionRun.setLocalDate(LocalDateTime.now());

        serviceFunctionRun.save(newFunctionRun);
        serviceDataChange.save(dataChange);
    }


}
