package com.bsi.app.model;

import javax.persistence.*;

//table in DB - users

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 40)
    private String email;

    @Column(nullable = false, length = 40)
    private String login;

    @Column(nullable = false, length = 512)
    private String password;

    @Column(length = 20)
    private String salt;

    @Column(length = 5)
    private boolean isPasswordKeptAsHash;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean getIsPasswordKeptAsHash() {
        return isPasswordKeptAsHash;
    }

    public void setIsPasswordKeptAsHash(boolean passwordKeptAsHash) {
        this.isPasswordKeptAsHash = passwordKeptAsHash;
    }
}
