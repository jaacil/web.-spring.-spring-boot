package com.bsi.app.model;

import javax.persistence.*;

//table in DB - sites

@Entity
@Table(name = "sites")
public class Sites {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 40)
    private String site;

    @Column(nullable = false, length = 40)
    private String password;

    @Column(length = 512)
    private String description;

    @ManyToOne(targetEntity = User.class)
    private User user;

    public Sites() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Sites{" +
                "id=" + id +
                ", site='" + site + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                ", user=" + user +
                '}';
    }
}
