package com.bsi.app.repo;

import com.bsi.app.model.Sites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//repository for sites table
//Allows to menage and perform operations on data. This repository is connected with data model - sites

@Repository
public interface SitesRepo extends JpaRepository<Sites, Long> {

    public Optional<Sites> findById(Long id);
    public List<Sites> findByUserId(Long userId);
    public List<Sites> findAllByUserId(Long userid);
    public void deleteById(Long id);

}
