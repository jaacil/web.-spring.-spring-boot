package com.bsi.app.repo;


import com.bsi.app.model.Function;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctionRepo extends JpaRepository<Function, Long> {

    public Function findByFunctionName(String name);

}
