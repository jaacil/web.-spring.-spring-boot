package com.bsi.app.repo;

import com.bsi.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//repository for user table
//Allows to menage and perform operations on data. This repository is connected with data model - users

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    public List<User> findByEmail(String email);
    public List<User> findByLogin(String login);

}
