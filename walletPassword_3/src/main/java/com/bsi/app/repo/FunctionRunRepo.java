package com.bsi.app.repo;

import com.bsi.app.model.FunctionRun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FunctionRunRepo extends JpaRepository<FunctionRun, Long> {

    public List<FunctionRun> findAllByUserId(Long id);

}
