package com.bsi.app.repo;

import com.bsi.app.model.DataChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataChangeRepo extends JpaRepository<DataChange, Long> {

    public List<DataChange> findAllByUserId(Long id);

}
