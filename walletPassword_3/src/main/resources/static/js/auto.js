function showAuto() {
    $(document).ready(function () {
        if (sessionStorage.getItem('zm') !== 'true') {
            $('#myModal').modal('show');
            sessionStorage.setItem('zm', 'true');
        }
    });
}
